#pragma once
#include <bitset>
#include <functional>

#include "Display.h"
#include "Keyboard.h"
#include "Audio.h"


enum class HaltReason
{
	kWaitForKey,  // Instruction Fx0A, halt_data_ is the register to store the key in
	kDebug,       // For debugging purposes
	kTimingBuffer, // For slowing down the rate at which the emulator executes instructions, halt_data_ is timing delay
	kUnknownOpcode, // In the case of an opcode being decoded which isn't knowm.
	kUnspecified
};


///<summary> Contains code to handle the reading, decoding and execution of instructions </summary>
class Processor
{
public:
	void CycleProcessor();
	void Tick(); //Runs at 60hz, handles decrementing timers and halting execution

	Processor();
	~Processor();
	std::bitset<8> system_memory_[4096]; 

	Display display_;

private:
	Keyboard keyboard_;
	Audio audio_;
	int EightBitToSignedInt(std::bitset<8> bitset);

	
	std::bitset<8> system_registers_[16];

	std::bitset<16> i_register_; // Special register, typically used to store memory addresses.
	int delay_timer_ = 0; // Decremented by 1 at a rate of 60hz.

	std::bitset<16> program_counter_ = 512; 
	
	int stack_pointer_ = -1; //Call instruction iterates the stack pointer before reading from the top of the stack.
	std::bitset<16> stack_[16];

	bool halted_ = false; //If true, then CycleProcessor will do nothing.
	HaltReason halt_reason_ = HaltReason::kUnspecified;
	int halt_data_ = 0; //Stores any extra data about the halt that may be needed. 

/*  Beginning of instruction declerations.Function names are the mnemonic.
	where two or more instructions share the same mnemonic,
	the name will be followed by the first hexadecimal character of the instruction 
	List of instruction mnemonics: http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#3.1 */

	//void SYS(); : This instruction is typically ignored by modern emulators.
	void CLS();
	void RET();
	void JP(std::bitset<12> addr);
	void CALL(std::bitset<12> addr);
	void SE3(int x, std::bitset<8> byte);
	void SNE4(int x, std::bitset<8> byte);
	void SE5(int x, int y);
	void LD6(int x, std::bitset<8> byte);
	void ADD7(int x, std::bitset<8> byte);
	void LD8(int x, int y);
	void OR(int x, int y);
	void AND(int x, int y);
	void XOR(int x, int y);
	void ADD8(int x, int y);
	void SUB(int x, int y);
	void SHR(int x, int y); // the y register isn't used in base Chip-8
	void SUBN(int x, int y);
	void SHL(int x, int y); // the y register isn't used in base Chip-8
	void SNE9(int x, int y);
	void LDA(std::bitset<12> addr);
	void JPB(std::bitset<12> addr);
	void RND(int x, std::bitset<8> byte);

	void DRW(int x, int y, std::bitset<4> nibble);
	void SKP(int x);
	void SKNP(int x);

	void LDF7(int x);
	void LDFA(int x);
	void LDF15(int x);
	void LDF18(int x);

	void ADDF(int x);

	void LDF29(int x);
	void LDF33(int x); 
	void LDF55(int x);
	void LDF65(int x);

	//Instruction decoding
	
	typedef void (Processor::*FunctionPointer)(std::bitset<8>, std::bitset<8>);
	FunctionPointer first_stage_decoding[16] = { &Processor::decode_0, &Processor::decode_1, &Processor::decode_2, &Processor::decode_3 , &Processor::decode_4, &Processor::decode_5,
												 &Processor::decode_6, &Processor::decode_7, &Processor::decode_8, &Processor::decode_9, &Processor::decode_A, &Processor::decode_B,
												 &Processor::decode_C, &Processor::decode_D, &Processor::decode_E, &Processor::decode_F
	};
	

	//First stage decoding functions (15 in total)
	void decode_0(std::bitset<8> higher, std::bitset<8> lower);
	void decode_1(std::bitset<8> higher, std::bitset<8> lower);
	void decode_2(std::bitset<8> higher, std::bitset<8> lower);
	void decode_3(std::bitset<8> higher, std::bitset<8> lower);
	void decode_4(std::bitset<8> higher, std::bitset<8> lower);
	void decode_5(std::bitset<8> higher, std::bitset<8> lower);
	void decode_6(std::bitset<8> higher, std::bitset<8> lower);
	void decode_7(std::bitset<8> higher, std::bitset<8> lower);
	void decode_8(std::bitset<8> higher, std::bitset<8> lower);
	void decode_9(std::bitset<8> higher, std::bitset<8> lower);
	void decode_A(std::bitset<8> higher, std::bitset<8> lower);
	void decode_B(std::bitset<8> higher, std::bitset<8> lower);
	void decode_C(std::bitset<8> higher, std::bitset<8> lower);
	void decode_D(std::bitset<8> higher, std::bitset<8> lower);
	void decode_E(std::bitset<8> higher, std::bitset<8> lower);
	void decode_F(std::bitset<8> higher, std::bitset<8> lower);
};


