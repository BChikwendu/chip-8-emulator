#include "CartridgeLoader.h"

bool CartridgeLoader::LoadRom(std::string rom_path, Processor* processor_)
{
	std::ifstream stream(rom_path,std::ios::binary);
	if (stream) {


		stream.ignore(std::numeric_limits<std::streamsize>::max()); //Ignore everything in the file, better than writing 9999
		std::streamsize length = stream.gcount();
		stream.clear(); //Clear the error state, i.e get rid of the eof flag set by reading everything with ignore()
		stream.seekg(0, std::ios_base::beg);
		
		char* files_bytes = new char[length];
		stream.read(files_bytes, length);

		//convert files_bytes from char* to std::vector< std::bitset<8> >
		
		

		for (size_t i = 0; i < length; i++)
		{
			processor_->system_memory_[i + 512] = files_bytes[i];
		/*	std::cout << std::setfill('0') << std::setw(2) << std::hex << (int)((unsigned char)files_bytes[i]);
			std::cout << " ";
			if ((i + 1) % 16 == 0)
				std::cout << "\n";*/
		}
		
		return true; 
	}
	else {
		return false;
	}
}

