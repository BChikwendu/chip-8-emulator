#include "Keyboard.h"

Keyboard::Keyboard()
{
    //Fill out the key mapping
    key_mapping_[1] = sf::Keyboard::Numpad7;
    key_mapping_[2] = sf::Keyboard::Numpad8;
    key_mapping_[3] = sf::Keyboard::Numpad9;
    key_mapping_[0xC] = sf::Keyboard::Subtract;

    key_mapping_[4] = sf::Keyboard::Numpad4;
    key_mapping_[5] = sf::Keyboard::Numpad5;
    key_mapping_[6] = sf::Keyboard::Numpad6;
    key_mapping_[0xD] = sf::Keyboard::Add;

    key_mapping_[7] = sf::Keyboard::Numpad1;
    key_mapping_[8] = sf::Keyboard::Numpad2;
    key_mapping_[9] = sf::Keyboard::Numpad3;
    key_mapping_[0xE] = sf::Keyboard::I;

    key_mapping_[0xA] = sf::Keyboard::O;
    key_mapping_[0] = sf::Keyboard::Numpad0;
    key_mapping_[0xB] = sf::Keyboard::P;
    key_mapping_[0xF] = sf::Keyboard::U;
}

bool Keyboard::IsKeyDown(int key)
{
    return sf::Keyboard::isKeyPressed(key_mapping_[key]);
}

std::vector<int> Keyboard::GetPressedKeys()
{
    std::vector<int> pressed_keys;
    for (size_t i = 0; i < key_mapping_.size(); i++)
    {
        bool key_down = IsKeyDown(i);
        if (key_down) {
            pressed_keys.push_back(i);
        }
    }
    return pressed_keys;
}
