#include "CartridgeLoader.h"

int main(int argc, char *argv[])
{

    //TODO: Broken Opcodes 
    const int kPixelSize = 8;
    const int kDisplayWidth = kPixelSize * 64;
    const int KDispayHeight = kPixelSize * 32;
 
    //Read program options

    std::string rom_name;
    int frame_rate;
    switch (argc)
    {
    case 3:
        rom_name = argv[1];
        frame_rate = std::atoi(argv[2]);
        break;
    case 2:
        rom_name = argv[1];
        break;

    default:
        rom_name = "rom.c8";
        frame_rate = 60;
        break;
    }



    //Chip-8 display size is 64x32 pixels. Each hardware pixel of the Chip-8 will be an 8x8 square on our screen. The extra 64 pixels is for debugging.
    sf::RenderWindow window(sf::VideoMode(kDisplayWidth, KDispayHeight), "Chip-8 Emulator"); 
    window.setFramerateLimit(60); //Cap drawing and updates to 60hz, which may result in games seeming "slow" by modern standards.
    srand(time(NULL));
    Processor *proccessor_ = new Processor();
   

    CartridgeLoader cl;
    if (!cl.LoadRom(rom_name, proccessor_)) {
        std::cout << "\nFailed to load ROM : " << rom_name << "\n";

        delete proccessor_;
        window.close();
        return 0;
    }
    
    
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed) {
                delete proccessor_;
                window.close();
            }

            if (event.type == sf::Event::KeyReleased)
            {
                
                //proccessor_->CycleProcessor();
            }

        }

        
        proccessor_->Tick();
        proccessor_->CycleProcessor();
        //proccessor_->CycleProcessor();
        window.clear();

        proccessor_->display_.DrawScreenPixels(&window, kPixelSize);

        window.display();
    }

    return 0;
}