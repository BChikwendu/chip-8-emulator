#pragma once
#include <map>

#include <SFML/Window.hpp>

///<summary> Contains code to handle the reading of user input </summary>
class Keyboard
{
public:
	Keyboard();
	bool IsKeyDown(int key);
	std::vector<int> GetPressedKeys();


private:
	std::map<int, sf::Keyboard::Key> key_mapping_; //Mapping between a 4bit hexadecimal keycode, and a physical key.
};

