#pragma once
#include <iostream>
#include <fstream>
#include <limits>
#include <iomanip>

#include "Processor.h"
/// <summary>
/// Loads data from a cartridge file into the processor's memory.
/// </summary>
class CartridgeLoader
{
public:
	bool LoadRom(std::string rom_, Processor *processor_);
	

private:

};

