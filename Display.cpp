#include "Display.h"

void Display::ClearScreen()
{
    for (size_t i = 0; i < 64; i++)
    {

        for (size_t j = 0; j < 32; j++)
        {
            display_pixels_[i][j] = 0;
        }
    }
    
}

bool Display::ReportingXorDraw(int x, int y, int bit)
{
    int x_pixel_ = x % 64;
    int y_pixel_ = y % 32;
    
    int prev_state = display_pixels_[x_pixel_][y_pixel_];

    display_pixels_[x_pixel_][y_pixel_] = display_pixels_[x_pixel_][y_pixel_] ^ (bool)bit;

    if (display_pixels_[x_pixel_][y_pixel_] == 0 && prev_state == 1) {
        return true;
    }
    else {
        return false;
    }
    
}

void Display::DrawScreenPixels(sf::RenderWindow* window, int kPixelSize)
{
    sf::RectangleShape screen_pixel;
    screen_pixel.setSize(sf::Vector2f(kPixelSize, kPixelSize));

    for (size_t x = 0; x < 64; x++)
    {
        for (size_t y = 0; y < 32; y++)
        {
            screen_pixel.setPosition(sf::Vector2f(x * kPixelSize, y * kPixelSize));
            if (display_pixels_[x][y] == 1) {
                screen_pixel.setFillColor(sf::Color::White);
            }
            else {
                screen_pixel.setFillColor(sf::Color::Black);
            }
            window->draw(screen_pixel);
        }

    }

}
