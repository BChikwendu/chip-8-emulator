#pragma once
#include <SFML/Graphics.hpp>

///<summary> Contains code to handle the updating and drawing of pixels to the screen </summary>
class Display
{

public:
	void ClearScreen();
	/// <summary>
	/// Xor's the pixel at location x, y. Locations wrap around.
	/// </summary>
	/// <returns>Whether any pixel was erased</returns>
	bool ReportingXorDraw(int x, int y, int bit);



	void DrawScreenPixels(sf::RenderWindow *window, int kPixelSize);
private:
	bool display_pixels_[64][32];
	
};

