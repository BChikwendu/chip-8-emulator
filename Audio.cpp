#include "Audio.h"

Audio::Audio()
{
	if (tone_buffer_->loadFromFile("Sounds/tone.wav")) {
		succesful_load = true;
		tone_->setBuffer(*tone_buffer_);
	} 
}

void Audio::SetSoundTimer(int time)
{
	sound_timer_ = time;
}

void Audio::TickTimer()
{
	if (succesful_load) {
		if (sound_timer_ > 0) {
			sound_timer_ -= 1;
			if (tone_->getStatus() == sf::SoundSource::Status::Paused || tone_->getStatus() == sf::SoundSource::Status::Stopped) {
				tone_->play();
			}
		}
		else if (tone_->getStatus() == sf::SoundSource::Status::Playing) {
			tone_->stop();
		}

	}
}

void Audio::CleanUp()
{
	if (!(tone_->getStatus() == sf::SoundSource::Status::Stopped) && succesful_load) {
		tone_->stop();	
	}
	delete tone_;
	delete tone_buffer_;
}
