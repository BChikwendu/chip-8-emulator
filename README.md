# Chip 8 Emulator
This is an emulator capable of running Chip-8 Roms. It was written in c++ and uses SFML to handle windowing, graphics and audio.

# Usage
By default, without any command line arguments specified, it will look for a rom called "rom.c8" and run that at an emulation speed of 60fps. 
You can specify a different rom to be read with the format:  
```Chip-8-Emulator romname```  
You can also specify a target emulation speed using the format:  
`Chip-8-Emulator romname targetfps`.  
Note that it is not possible to specify only an emulation speed to run at, if you want to run a ROM at a specific emulation speed you must specify the ROM's filename. 

# Code Style
Where reasonable, the code takes inspiration from Google's C++ style guide, with deviations where I saw fit, such as using #pragma-once over include guards.

# Example Usage  
`Chip-8-Emulator pong.rom 1440`  

# Other Notes: 

### Execution Speed
Please note that the default emulation rate of 60fps, while more accurate to the initial intention, may seem extremely slow or flickery by modern standards. This is intended and can easily be solved by setting an emulation rate in the mid to high thousands, though that may mean that timers are not 1:1 accurate with the original duration the developers of your ROM intended.

### Terminology
While programs like mine are typically referred to as "Chip 8 Emulators", this isn't strictly correct. There was never an original physical console with a CPU capable of natively running Chip 8 machine code. Even back when it was first invented in the 70s, it was run on Virtual Machines / Interpreters on microcomputers.  
The tendency to refer to them as emulators comes from the fact that you program a chip-8 virtual machine in the same fashion to a regular emulator, and the same techniques can be applied.

### Operation Decoding
Due to the simplicity of Chip-8, typically a massive switch case is used to decode instructions.  
I preferred to use instead function pointers, which is a more scalable and quicker way of decoding.
