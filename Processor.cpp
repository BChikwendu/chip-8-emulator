#include "Processor.h"

/// <summary>
/// Read, decode, and execute the next instruction.
/// </summary>
void Processor::CycleProcessor()
{

	//std::cout << "Exec ins @: " << std::hex << program_counter_.to_ulong() << "\n";

	//Read next instruction. 
	std::bitset<8> next_instruction_higher = system_memory_[program_counter_.to_ulong()];
	std::bitset<8> next_instruction_lower = system_memory_[program_counter_.to_ulong() + 1];
	//Iterate program counter (by 2)
	program_counter_ = std::bitset<16>(program_counter_.to_ulong() + 2);

	//First 4 bits (MSB) will be used as index into an array of function pointers 
	int func_index = (next_instruction_higher >> 4).to_ulong();

	//std::cout << "decoded: " << std::hex <<func_index << "x" << next_instruction_lower.to_ulong() << "\n";
	//std::cout << "New Pc before instruction: " << std::hex << program_counter_.to_ulong() << "\n";

	(*this.*first_stage_decoding[func_index])(next_instruction_higher, next_instruction_lower);

	/*for (size_t i = 0; i < 16; i++)
	{
		std::cout << "Reg[" << i << "] : " << system_registers_[i] << "\n";
	}*/
	//std::cout << "I_Reg: " << i_register_ << "\n";
	//std::cout << "New PC after instruction: " <<std::hex << program_counter_.to_ulong() << "\n";
}

void Processor::Tick()
{
	if (delay_timer_ > 0) {
		delay_timer_ -= 1;
	}

	audio_.TickTimer();

	if (halted_) {
		switch (halt_reason_)
		{
		case HaltReason::kWaitForKey: {
			std::vector<int> pressed_keys = keyboard_.GetPressedKeys();
			if (pressed_keys.size() > 0) {
				halted_ = false;
				system_registers_[halt_data_] = pressed_keys[0];
			}
			break;
		}

		default:
			break;
		}

	}
}

Processor::Processor()
{	
	//0x000 to 0x1FF of memory should have the sprite data for characters '0' -> 'F' in memory, we must set this up;

	//Character data for '0'
	system_memory_[0] = 0xF0;
	system_memory_[1] = 0x90;
	system_memory_[2] = 0x90;
	system_memory_[3] = 0x90;
	system_memory_[4] = 0xF0;

	//Character data for '1' etc
	system_memory_[5] = 0x20;
	system_memory_[6] = 0x60;
	system_memory_[7] = 0x20;
	system_memory_[8] = 0x20;
	system_memory_[9] = 0x70;

	//'2'
	system_memory_[10] = 0xF0;
	system_memory_[11] = 0x10;
	system_memory_[12] = 0xF0;
	system_memory_[13] = 0x80;
	system_memory_[14] = 0xF0;
	
	//'3'
	system_memory_[15] = 0xF0;
	system_memory_[16] = 0x10;
	system_memory_[17] = 0xF0;
	system_memory_[18] = 0x10;
	system_memory_[19] = 0xF0;
	//'4'
	system_memory_[20] = 0x90;
	system_memory_[21] = 0x90;
	system_memory_[22] = 0xF0;
	system_memory_[23] = 0x10;
	system_memory_[24] = 0x10;

	//'5'
	system_memory_[25] = 0xF0;
	system_memory_[26] = 0x80;
	system_memory_[27] = 0xF0;
	system_memory_[28] = 0x10;
	system_memory_[29] = 0xF0;
	
	//'6'
	system_memory_[30] = 0xF0;
	system_memory_[31] = 0x80;
	system_memory_[32] = 0xF0;
	system_memory_[33] = 0x90;
	system_memory_[34] = 0xF0;

	//'7'
	system_memory_[35] = 0xF0;
	system_memory_[36] = 0x10;
	system_memory_[37] = 0x20;
	system_memory_[38] = 0x40;
	system_memory_[39] = 0x40;

	//'8'
	system_memory_[40] = 0xF0;
	system_memory_[41] = 0x90;
	system_memory_[42] = 0xF0;
	system_memory_[43] = 0x90;
	system_memory_[44] = 0xF0;

	//'9'
	system_memory_[45] = 0xF0;
	system_memory_[46] = 0x90;
	system_memory_[47] = 0xF0;
	system_memory_[48] = 0x10;
	system_memory_[49] = 0xF0;

	//'A'
	system_memory_[50] = 0xF0;
	system_memory_[51] = 0x90;
	system_memory_[52] = 0xF0;
	system_memory_[53] = 0x90;
	system_memory_[54] = 0x90;

	//'B'
	system_memory_[55] = 0xE0;
	system_memory_[56] = 0x90;
	system_memory_[57] = 0xE0;
	system_memory_[58] = 0x90;
	system_memory_[59] = 0xE0;

	//'C'
	system_memory_[60] = 0xF0;
	system_memory_[61] = 0x80;
	system_memory_[62] = 0x80;
	system_memory_[63] = 0x80;
	system_memory_[64] = 0xF0;

	//'D'
	system_memory_[65] = 0xE0;
	system_memory_[66] = 0x90;
	system_memory_[67] = 0x90;
	system_memory_[68] = 0x90;
	system_memory_[69] = 0xE0;

	//'E'
	system_memory_[70] = 0xF0;
	system_memory_[71] = 0x80;
	system_memory_[72] = 0xF0;
	system_memory_[73] = 0x80;
	system_memory_[74] = 0xF0;

	//'F'
	system_memory_[75] = 0xF0;
	system_memory_[76] = 0x80;
	system_memory_[77] = 0xF0;
	system_memory_[78] = 0x80;
	system_memory_[79] = 0x80;
}

Processor::~Processor()
{
	audio_.CleanUp();
}

int Processor::EightBitToSignedInt(std::bitset<8> bitset)
{
	return (int)bitset.to_ulong();
}

void Processor::CLS()
{
	display_.ClearScreen();
}

void Processor::RET()
{

	program_counter_ = stack_[stack_pointer_];
	stack_pointer_--;

}

void Processor::JP(std::bitset<12> addr)
{
	program_counter_ = std::bitset<16>(addr.to_ullong());
}

void Processor::CALL(std::bitset<12> addr)
{
	stack_pointer_++;
	stack_[stack_pointer_] = program_counter_;
	program_counter_ = std::bitset<16>(addr.to_ullong());
}

void Processor::SE3(int x, std::bitset<8> byte)
{
	if (system_registers_[x] == byte) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);

	}
}

void Processor::SNE4(int x, std::bitset<8> byte)
{
	if (system_registers_[x] != byte) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);

	}
}

void Processor::SE5(int x, int y)
{
	if (system_registers_[x] == system_registers_[y]) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);
	}
}

void Processor::LD6(int x, std::bitset<8> byte)
{
	system_registers_[x] = byte;
}

void Processor::ADD7(int x, std::bitset<8> byte)
{
	system_registers_[x] = system_registers_[x].to_ulong() + byte.to_ulong();
}

void Processor::LD8(int x, int y)
{
	system_registers_[x] = system_registers_[y];
}

void Processor::OR(int x, int y)
{
	system_registers_[x] = system_registers_[x] | system_registers_[y];
}

void Processor::AND(int x, int y)
{
	system_registers_[x] = system_registers_[x] & system_registers_[y];
}

void Processor::XOR(int x, int y)
{
	system_registers_[x] = system_registers_[x] ^ system_registers_[y];
}

void Processor::ADD8(int x, int y)
{
	std::bitset<12> x_extended (system_registers_[x].to_ulong());
	std::bitset<12> y_extended (system_registers_[y].to_ulong());

	std::bitset<12> z = x_extended.to_ulong() + y_extended.to_ulong();

	if (z.to_ulong() > 255) {
		system_registers_[0xf] = 1;
	}
	else {
		system_registers_[0xf] = 0;
	}

	for (int i = 0; i < 8; i++)
	{
		system_registers_[x][i] = z[i];
	}
	

}

void Processor::SUB(int x, int y)
{
	int x_signed_ = (int) system_registers_[x].to_ulong();
	int y_signed_ = (int) system_registers_[y].to_ulong();

	if (x_signed_ > y_signed_) {
		system_registers_[0xf] = 1;
	}
	else {
		system_registers_[0xf] = 0;
	}

	int z = x_signed_ - y_signed_;
	unsigned long z_unsigned = z;
	system_registers_[x] = z_unsigned;
}

void Processor::SHR(int x, int y)
{
	if (system_registers_[x][0] == 1) {
		system_registers_[0xf] = 1;
	}
	else {
		system_registers_[0xf] = 0;
	}
	system_registers_[x] = system_registers_[x] >> 1;
	

}

void Processor::SUBN(int x, int y)
{
	int x_signed_ = (int)system_registers_[x].to_ulong();
	int y_signed_ = (int)system_registers_[y].to_ulong();

	if (x_signed_ < y_signed_) {
		system_registers_[0xf] = 1;
	}
	else {
		system_registers_[0xf] = 0;
	}

	int z =  y_signed_ - x_signed_;
	unsigned long z_unsigned = z;
	system_registers_[x] = z_unsigned;
}

void Processor::SHL(int x, int y)
{

	if (system_registers_[x][7] == 1) {

		system_registers_[0xf] = 1;
	}
	else {
		system_registers_[0xf] = 0;
	}
	system_registers_[x] = system_registers_[x] << 1;


}

void Processor::SNE9(int x, int y)
{
	if (system_registers_[x] != system_registers_[y]) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);
	}
}

void Processor::LDA(std::bitset<12> addr)
{
	for (int i = 0; i < 13; i++)
	{
		i_register_[i] = addr[i];
	}
	
}

void Processor::JPB(std::bitset<12> addr)
{
	unsigned long final_address_ = addr.to_ulong() + system_registers_[0].to_ulong();
	program_counter_ = final_address_;
}

void Processor::RND(int x, std::bitset<8> byte)
{
	std::bitset<8> random_byte_ = (rand() % 256); //Random number between 0 and 255 (inclusive)
	system_registers_[x] = byte & random_byte_;
}

void Processor::DRW(int x, int y, std::bitset<4> nibble)
{

	//Takes memory location stored in register i to be top left of sprite
	//Read [nibble] bytes from memory starting at i, 


	unsigned int x_coord = system_registers_[x].to_ulong();
	unsigned int y_coord = system_registers_[y].to_ulong();

	//If no collisions are found, keep f register at zero
	system_registers_[0xf] = 0;

	for (int j = 0; j < nibble.to_ulong(); j++)
	{
		

		//Get address : Base Address (i register) + j

		int base_addy = i_register_.to_ulong();
		int final_addy = base_addy + j;

		std::bitset<8> drawn_byte_ = system_memory_[final_addy]; //TODO : working correctly

		//Draw the bytes. Most significant bit is leftmost pixel
		for (int k = 0; k < 8; k++)
		{
			
			bool erased = display_.ReportingXorDraw(x_coord + (7 - k), y_coord + j, drawn_byte_[k]);
			if (erased) {
				system_registers_[0xf] = 1;
			}
			
		}

	}
}

void Processor::SKP(int x)
{
	//Get the keycode stored in register x
	int keycode = system_registers_[x].to_ulong();
	if (keyboard_.IsKeyDown(keycode)) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);
	}
}

void Processor::SKNP(int x)
{
	//Get the keycode stored in register x
	int keycode = system_registers_[x].to_ulong();
	if (!keyboard_.IsKeyDown(keycode)) {
		program_counter_ = std::bitset<16>(program_counter_.to_ullong() + 2);
	}
}

void Processor::LDF7(int x)
{
	system_registers_[x] = delay_timer_;
}

void Processor::LDFA(int x)
{
	halted_ = true;
	halt_reason_ = HaltReason::kWaitForKey;
	halt_data_ = x;
}

void Processor::LDF15(int x)
{
	delay_timer_ = system_registers_[x].to_ulong();
}

void Processor::LDF18(int x)
{
	audio_.SetSoundTimer (system_registers_[x].to_ulong());
}

void Processor::ADDF(int x)
{
	i_register_ = i_register_.to_ulong() + system_registers_[x].to_ulong();
}

void Processor::LDF29(int x)
{
	//Get the value of register Vx, then multiply by 5
	//convert that to an address in binary

	i_register_ = (system_registers_[x].to_ulong() * 5);
}

void Processor::LDF33(int x)
{
	int decimal_value = system_registers_[x].to_ulong();
	int ones_digit = decimal_value % 10;
	decimal_value -= ones_digit;

	int tens_digit = (decimal_value % 100) / 10;
	decimal_value -= tens_digit;

	int hundreds_digit = decimal_value / 100;

	//hundreds digit in memory at location in I, the tens digit at location I+1, and the ones digit at location I+2
	system_memory_[i_register_.to_ulong()] = hundreds_digit;
	system_memory_[i_register_.to_ulong() + 1] = tens_digit;
	system_memory_[i_register_.to_ulong() + 2] = ones_digit;


}

void Processor::LDF55(int x)
{
	int base_address = i_register_.to_ulong();
	for (size_t j = 0; j < (x + 1); j++)
	{
		system_memory_[base_address + j] = system_registers_[j];
	}
}

void Processor::LDF65(int x)
{
	int base_address = i_register_.to_ulong();
	for (size_t j = 0; j < (x + 1); j++)
	{
		 system_registers_[j] = system_memory_[base_address + j];
	}

}

void Processor::decode_0(std::bitset<8> higher, std::bitset<8> lower)
{	
	if (lower[3] == 0) {
		CLS();
	
	}
	else if (lower[3] == 1) {
		RET();
	}
	else {
		//Zero decoded incorrectly
		halted_ = true;
		halt_reason_ = HaltReason::kUnknownOpcode;
		halt_data_ = 0;
	}
}

void Processor::decode_1(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<12> address;
	address[11] = higher[3];
	address[10] = higher[2];
	address[9] = higher[1];
	address[8] = higher[0];

	for (int i = 7; i > -1; i--)
	{
		address[i] = lower[i];
	}

	JP(address);
}

void Processor::decode_2(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<12> address;
	address[11] = higher[3];
	address[10] = higher[2];
	address[9] = higher[1];
	address[8] = higher[0];

	for (int i = 7; i > -1; i--)
	{
		address[i] = lower[i];
	}

	CALL(address);
}

void Processor::decode_3(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();
	SE3(x, lower);
	
}

void Processor::decode_4(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();
	SNE4(x, lower);
}

void Processor::decode_5(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	std::bitset<8> higher_mask = 0b11110000;
	int y = ((lower & higher_mask) >> 4).to_ulong();

	SE5(x, y);
}

void Processor::decode_6(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	LD6(x, lower);

}

void Processor::decode_7(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	ADD7(x, lower);
}

void Processor::decode_8(std::bitset<8> higher, std::bitset<8> lower)
{
	//The 9 different 8___ instructions are only differentiated by their least significant 4 bits.
	//Each of them take x (LS 4 bits of the higher 8), and y (MS 4 bits of the lower 8)

	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	std::bitset<8> higher_mask = 0b11110000;
	int y = ((lower & higher_mask) >> 4).to_ulong();

	int switch_bit = (lower & lower_mask).to_ulong();

	switch (switch_bit)
	{

	case 0:
		LD8(x, y);
		break;

	case 1:
		OR(x, y);
		break;
	case 2:
		AND(x, y);
		break;
	case 3:
		XOR(x, y);
		break;
	case 4:
		ADD8(x, y);
		break;
	case 5:
		SUB(x, y);
		break;
	case 6:
		SHR(x, y);
		break;
	case 7:
		SUBN(x, y);
		break;
	case 0xE:
		SHL(x, y);
		break;
	default:
		//Eight decoded incorrectly
		halted_ = true;
		halt_reason_ = HaltReason::kUnknownOpcode;
		halt_data_ = 8;
		break;
	}
}

void Processor::decode_9(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	std::bitset<8> higher_mask = 0b11110000;
	int y = ((lower & higher_mask) >> 4).to_ulong();

	SNE9(x, y);
}

void Processor::decode_A(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<12> address;
	address[11] = higher[3];
	address[10] = higher[2];
	address[9] = higher[1];
	address[8] = higher[0];

	for (int i = 7; i > -1; i--)
	{
		address[i] = lower[i];
	}

	LDA(address);
}

void Processor::decode_B(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<12> address;
	address[11] = higher[3];
	address[10] = higher[2];
	address[9] = higher[1];
	address[8] = higher[0];

	for (int i = 7; i > -1; i--)
	{
		address[i] = lower[i];
	}
	JPB(address);
}

void Processor::decode_C(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();
	RND(x, lower);
}

void Processor::decode_D(std::bitset<8> higher, std::bitset<8> lower)
{

	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	std::bitset<8> higher_mask = 0b11110000;
	int y = ((lower & higher_mask) >> 4).to_ulong();

	std::bitset<4> n = (lower & lower_mask).to_ulong();

	DRW(x, y, n);

}

void Processor::decode_E(std::bitset<8> higher, std::bitset<8> lower)
{
	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();
	int switch_bit = (lower & lower_mask).to_ulong();

	if (switch_bit == 1) {

		SKNP(x);
	}
	else {
		SKP(x);
	}
}

void Processor::decode_F(std::bitset<8> higher, std::bitset<8> lower)
{
	int switch_byte = lower.to_ulong();

	std::bitset<8> lower_mask = 0b00001111;
	int x = (higher & lower_mask).to_ulong();

	switch (switch_byte)
	{

	case 0x07:
		LDF7(x);
		break;
	case 0x0A:
		LDFA(x);
		break;
	case 0x15:
		LDF15(x);
		break;
	case 0x18:
		LDF18(x);
		break;
	case 0x1E:
		ADDF(x);
		break;
	case 0x29:
		LDF29(x); 
		break;
	case 0x33:
		LDF33(x);
		break;
	case 0x55:
		LDF55(x);
		break;
	case 0x65:
		LDF65(x);
		break;
	default:
		//F decoded incorrectly
		halted_ = true;
		halt_reason_ = HaltReason::kUnknownOpcode;
		halt_data_ = 0xF;
		break;
	}

}
