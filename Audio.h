#pragma once

#include <SFML/Audio.hpp>


class Audio
{

public:
	Audio();
	void SetSoundTimer(int time);
	void TickTimer();
	void CleanUp(); // Clean up resources and ensure devices close
private:
	bool succesful_load = false; //If we cannot load a sound, we simply shouldn't play one, it's not critical.
	int sound_timer_;

	sf::SoundBuffer *tone_buffer_ = new sf::SoundBuffer();
	sf::Sound *tone_ = new sf::Sound();
};

